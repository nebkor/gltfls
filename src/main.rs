use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, Parser)]
#[command(author, version = "1", about = "")]
struct Cli {
    #[clap(help = "GLTF file to print info about")]
    pub gltf_file: PathBuf,
}

fn main() {
    let cli = Cli::parse();
    let gf = cli.gltf_file;
    let scenes = easy_gltf::load(gf).expect("Could not load {gf:?}");
    for (i, scene) in scenes.iter().enumerate() {
        println!(
            "SCENE {i}\nCameras: {}\nLights: {}\nModels: {}",
            scene.cameras.len(),
            scene.lights.len(),
            scene.models.len()
        );

        for (i, model) in scene.models.iter().enumerate() {
            println!("Model {i}: {}", model.mesh_name().unwrap_or("(no name)"));
        }
    }
}
